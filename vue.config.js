var ImageminPlugin = require('imagemin-webpack-plugin').default

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  lintOnSave: false,
  productionSourceMap: false,
  configureWebpack: {
    plugins: [
      new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i }),
    ]
  },
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].minify = false
        return args
      })
  },
  css: {
    modules: true,
    loaderOptions: {
      sass: {
        data: `@import "@/scss/variables.scss";`
      }
    }
  }
}
