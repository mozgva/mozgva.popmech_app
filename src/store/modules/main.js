export default {
  namespaced: true,
  state: {
    stage: 'main',
    
  },
  mutations: {
    changeStage (state, stage) {
      state.stage = stage;
    }
  },
  actions: {
    changeStage ({ commit }, stage) {
      commit('changeStage', stage);
    }
  }
}