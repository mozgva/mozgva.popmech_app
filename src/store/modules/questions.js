import questions from '../../questions.json'

export default {
  namespaced: true,
  state: {
    items: questions.map(quest => {
      return Object.assign(quest, {
        answered: false,
        valid: null,
      })
    }),
    selected: 0,
  },
  mutations: {
    selectQuest (state, idx) {
      state.selected = idx;
    },
    selectVariant (state, idx) {
      const quest = state.items[state.selected];

      const variant = quest.variants[idx];

      quest.valid = !!variant.valid;
      quest.answered = true;
    }
  },
  actions: {
    selectQuest ({ commit }, idx) {
      commit('selectQuest', idx);
    },
    selectVariant ({ commit }, idx) {
      commit('selectVariant', idx);
    }
  }
}