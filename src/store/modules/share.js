export default {
  namespaced: true,
  state: {
    active: false,    
  },
  mutations: {
    changeActive (state, active) {
      state.active = active;
    }
  },
  actions: {
    show ({ commit }) {
      commit('changeActive', true);
    },
    hide ({ commit }) {
      commit('changeActive', false);
    }
  }
}