import Vue from 'vue'
import Vuex from 'vuex'

import main from './modules/main'
import questions from './modules/questions'
import share from './modules/share'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    main,
    questions,
    share
  }
})
  
export default store