import Vue from 'vue'
import App from './App.vue'

import store from './store/'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App),
  store,
})

window.sendHeight = () => {
  const height = document.getElementById('container').offsetHeight;

  window.parent.postMessage(JSON.stringify({ from: 'mozgva', height }), '*');
}

document.addEventListener("DOMContentLoaded", () => { 
  window.sendHeight();
  
  window.addEventListener('resize', () => {
    window.sendHeight();
  });
});
